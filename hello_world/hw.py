#!/user/bin/env python

#import:
#from __future__ import print_function

color_red = chr(27) + "[1;31m"
color_reset = chr(27) + "[0m"

def main():
    """ main function docstring """
    i, b = 100, True
    print( color_red + "Hello World!" + color_reset )
    print( color_red + "Hello World!" , color_reset )

if __name__ == '__main__':
    main()
    